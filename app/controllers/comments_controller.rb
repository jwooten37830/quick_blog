class CommentsController < ApplicationController
  def create
    @post = Post.find(params[:post_id])
    @comment = @post.comments.create(comment_params)
    if @post.save
      redirect_to @post
    else
      flash[:error] = "Correct errors"
    end
  end
  
   private

        def comment_params
          params.require(:comment).permit(:commenter, :body, :post_id)
        end
end

# README

This README would normally document whatever steps are necessary to get the
application up and running.

Things you may want to cover:

* Ruby version
  2.2.2p95
* Rails version
  5.0.0

* System dependencies
  Had to install Postgress.app on el capitan, then:
  gem install pg -- --with-pg-config=/Applications/Postgres.app/Contents/Versions/9.5/bin/pg_config --with-pg-include='/Applications/Postgres.app/Contents/Versions/9.5/include/'
  so that the pg gem would get built correctly.  After installing Postgress.app, it launched the server.
  
* Configuration
  development and test databases use SQLite
  production currently uses PostgresSQL

* Database creation
  Adding production database using PostgresSQL for tesing.
  bundle install after the above gem install pg
  RAILS_ENV=production bundle exec rake db:create db:schema:load
* Database initialization

* In environments/production.rb edit false to true as below.
  config.assets.compile = true

  rails server -e production

* How to run the test suite

* Services (job queues, cache servers, search engines, etc.)

* Deployment instructions

* ...
